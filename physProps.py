"""
Physical Properties and functions for calculating the geostrophic wind.
"""

import numpy as np

#A dictionary of physical properties

physProps = {'pa' : 1e5 ,    #mean pressure
             'pb' : 200.,    #magnitude of the pressure variations
             'f' : 1e-4,     #Coriolis parameter
             'rho' : 1.,     #denisty
             'L' : 2.4e6,    #length scale of pressue variations (k)
             'ymin' : 0.,    #start of y domain
             'ymax' : 1e6}   #end of y domain
             
def pressure(y, props):
    ''' 
    The pressure of given y locations based on dictionary of physical 
    properties, props
    '''
    
    pa = props["pa"]
    pb = props["pb"]
    L = props["L"]
    
    return pa + pb*np.cos(y*np.pi/L)
    
    
def uGeoExact(y, props):
    '''
    The analytical geostrophic wind at given locations, y based on dictonary
    of physical properties, props
    '''
    
    pb = props["pb"]
    L = props["L"]
    rho = props["rho"]
    f = props["f"]
    
    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)

def geoWind(dpdy, props):
    '''
    The geostrophic wind as a function of pressure gradient based on dictionary
    of physical properties, props
    '''
    
    rho = props["rho"]
    f = props["f"]
    
    return -dpdy/(rho*f)


