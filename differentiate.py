"""
Functions for calculating gradients.
"""
import numpy as np

def gradient_2point(f, dx):
    '''
    The gradient of one-dimensional array f assuming points are distance dx 
    appart using 2-point differences. Returns an array the same size as f 
    '''
    
    #Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    
    #Two point differences at the end points
    dfdx[0] = (f[1]-f[0])/dx
    dfdx[-1] = (f[-1] - f[-2])/dx
       
    #Centred differences for the mid points
    for i in range(1, len(f)-1): 
         dfdx[i] = (f[i+1] - f[i-1])/(2*dx) 
    
    return dfdx



def gradient_2_3_point(f,dx):
    '''
    The gradient of one-dimensional array f assuming points are distance dx 
    appart using 2-point differences. End points calculated by 3 point 
    differences. Returns an array the same size a f 
    '''
    dfdx = np.zeros_like(f)
    
    #One-sided three point differences at the end points
    dfdx[0] = (4*f[1] - f[2] - 3*f[0])/(2*dx)
    dfdx[-1] = (3*f[-1] - 4*f[-2] + f[-3])/(2*dx)
       
    #Centred two-point differences for the mid points
    for i in range(1, len(f)-1): 
         dfdx[i] = (f[i+1] - f[i-1])/(2*dx) 
    
    return dfdx
