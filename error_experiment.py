"""
MTMW12 Assignment 3
Laura Risley
18th October 2021
Python code to investigate the order of the errors produced using numerical 
differentiation at y=N/2
"""

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *

def error_experiment():
    '''
    Vary the value of dy by increasing the number of intervals used in 
    gradient_2_point and plot them against the errors in a log log plot for 
    y is the mid point of the domain.
    Find the gradient of the log-log plot using polyfit
    '''
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    errors = np.zeros(10)
    dy = np.zeros(10)
    for i in range(1,11): #Loops of N = 10 to 100 to produce different dy'd and errors
        N = i*10
        n =int(N/2)
        y_array = np.linspace(ymin,ymax, N+1)
        y = y_array[int(N/2)]
        p = pressure(y_array, physProps)
        uExact = uGeoExact(y, physProps)
        dy[i-1] = (ymax - ymin)/(i*10)   #the length of the spacing
        dpdy = (p[n+1] - p[n-1])/(2*dy[i-1])
        u_2point = geoWind(dpdy, physProps)
        errors[i-1]=uExact - u_2point  

    #Create a log-log plot of error against dy/1000
    plt.loglog(dy/1000, errors)
    plt.xlabel('dy (km)')
    plt.ylabel('u, error (m/s)')
    plt.tight_layout()
    plt.title('Log-log plot of error against interval size')
    plt.savefig('geoWindErrorsexperiment.pdf.') 
    plt.show()
    
    poly = np.polyfit(np.log(dy/1000), np.log(errors), 1) #Use polyfit to find a line of best fit
    plt.plot(np.log(dy/1000), poly[1]+(poly[0]*(np.log(dy/1000))),\
             label = 'Polyfit') #Use polyfit tto plot the find a line of best fit
    plt.plot(np.log(dy/1000), np.log(errors), 'r--', label = 'Error')
    plt.legend(loc='best')
    plt.xlabel('log(dy) (km)')
    plt.ylabel('log(u), error (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindErrorsexperiment_polyfit.pdf.') 
    plt.show()
    print(f'The slope of the polyfit fitted line is equal to {poly[0]}.')
    
if __name__ == "__main__":
    error_experiment()