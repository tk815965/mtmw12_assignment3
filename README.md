**physProps.py**
This file contains all the physical properties and equations needed in the assignment

**differentiate.py**
This file contains the 2-point and 3-point differences formulas needed in the assignment.

All the following files require physProps and differentiate to be inported.

**geostrophicWind**
This runs code to find the 2-point differences for p and then uses this to find an estimate of u.
Then two plots are produced, the exact values against the numeric, and the errors.

**error_experiment.py**
This file runs the experiment that show what the order of accuracy of the errors is at y=N/2. 
The errors are plotted against the interval size, the gradient is found and this is the order.

**geostrophicWind_updated.py**
This is the same as geostrophicWind.py but updated to use the three-point differences for the end-ppoints.

**errors_experiment_updated.py**
This is the same as error_experiment.py but updated to investigate the error at y0 and yN for the three-point differences.