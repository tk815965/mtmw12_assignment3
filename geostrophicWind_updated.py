"""
MTMW12 Assignment 3
Laura Risley
18th October 2021
Python code to numerically differentiate the pressure in order to calculate
the geostrophic wind relation using 2-point differences and compare with the 
analytic solution and plot. This is more accurate than geoStrophicWind as the
end points have be calculated using 3-point differences.
"""

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *

def geostrophicWind_updated():
    '''
    Calculate the geostrophic wind analytically and numerically and plot using
    the updated gradient_2_3_point.
    '''
    
    N = 10  
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin)/N   
        
    y = np.linspace(ymin,ymax, N+1)
    
   
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps) 
                 
    #The pressure gradient and wind using using two point and three point 
    #differences
    
    dpdy = gradient_2_3_point(p, dy)
    u_2point = geoWind(dpdy, physProps)
  
    font = {'size' : 14}
    plt.rc('font',**font)

    
    #Plot the approximate and exact wind at y-points
    plt.plot(y/1000, uExact, 'k-', label = 'Exact')
    plt.plot(y/1000, u_2point, '*--', label='Two-point differences', \
             ms=12, markeredgewidth = 1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y (km)') 
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.title('Numerical and Analytical solutions of the wind')
    plt.savefig('geoWindupdated.pdf')
    plt.show()
    
    #Plot the errors
    plt.plot(y/1000, u_2point - uExact, 'ok-', label = 'Two-point differences',
             ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.xlabel('y (km)') 
    plt.ylabel('u, error (m/s)')
    plt.tight_layout()
    plt.title('Errors of the numerical solution')
    plt.savefig('geoWindErrorsupdated.pdf')
    plt.show()

if __name__ == "__main__":
    geostrophicWind_updated()