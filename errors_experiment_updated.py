"""
MTMW12 Assignment 3
Laura Risley
18th October 2021
Python code to investigate the order of the errors of the end-points of the domain
produced using numerical differentiation used in geostrophicWind_updated.py
"""

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *

def error_experiment_yN():
    '''
    Vary the value of dy by increasing the number of intervals used in 
    gradient_2_3_point and plot them against the errors in a log log plot for 
    y = ymax.
    '''
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    errors = np.zeros(10)
    dy = np.zeros(10)
    for i in range(1,11):
        N = i*10
        n =int(N/2)
        y_array = np.linspace(ymin,ymax, N+1)
        y = ymax
        p = pressure(y_array, physProps)
        uExact = uGeoExact(y, physProps)
        dy[i-1] = (ymax - ymin)/(i*10)   
        dpdy = (3*p[-1] - 4*p[-2] + p[-3])/(2*dy[i-1]) #three-piint difference
        u_2point = geoWind(dpdy, physProps)
        errors[i-1]= abs(uExact - u_2point)


    plt.loglog(dy/1000, errors)
    plt.xlabel('dy (km)')
    plt.ylabel('u, error (m/s)')
    plt.tight_layout()
    plt.title('Log-log plot of error against interval size for y=10^6')
    plt.savefig('geoWindErrorsexperimentyN.pdf.') 
    plt.show()
    
    poly = np.polyfit(np.log(dy/1000), np.log(errors), 1)
    plt.plot(np.log(dy/1000), poly[1]+(poly[0]*(np.log(dy/1000))), label = 'Polyfit')
    plt.plot(np.log(dy/1000), np.log(errors), 'r--', label = 'Error')
    plt.legend(loc='best')
    plt.xlabel('log(dy) (km)')
    plt.ylabel('log(u), error (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindErrorsexperiment_polyfityN.pdf.') 
    plt.show()
    print(f'The slope of the polyfit fitted line is equal to {poly[0]}.')
    print((np.log(errors[-1]/errors[0]))/(np.log(dy[-1]/dy[0])))


def error_experiment_y0():
    '''
    Vary the value of dy by increasing the number of intervals used in 
    gradient_2_3_point and plot them against the errors in a log log plot for 
    y = ymin.
    '''
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    errors = np.zeros(10)
    dy = np.zeros(10)
    for i in range(1,11):
        N = i*10
        n =int(N/2)
        y_array = np.linspace(ymin,ymax, N+1)
        y = ymin
        p = pressure(y_array, physProps)
        uExact = uGeoExact(y, physProps)
        dy[i-1] = (ymax - ymin)/(i*10)   
        dpdy = (4*p[1] - p[2] - 3*p[0])/(2*dy[i-1]) #three-piint difference
        u_2point = geoWind(dpdy, physProps)
        errors[i-1]= abs(uExact - u_2point)


    plt.loglog(dy/1000, errors)
    plt.xlabel('dy (km)')
    plt.ylabel('u, error (m/s)')
    plt.tight_layout()
    plt.title('Log-log plot of error against interval size for y=0')
    plt.savefig('geoWindErrorsexperimentyN.pdf.') 
    plt.show()
    
    poly = np.polyfit(np.log(dy/1000), np.log(errors), 1)
    plt.plot(np.log(dy/1000), poly[1]+(poly[0]*(np.log(dy/1000))), label = 'Polyfit')
    plt.plot(np.log(dy/1000), np.log(errors), 'r--', label = 'Error')
    plt.legend(loc='best')
    plt.xlabel('log(dy) (km)')
    plt.ylabel('log(u), error (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindErrorsexperiment_polyfity0.pdf.') 
    plt.show()
    print(f'The slope of the polyfit fitted line is equal to {poly[0]}.')
    print((np.log(errors[-1]/errors[0]))/(np.log(dy[-1]/dy[0])))
    
if __name__ == "__main__":
    error_experiment_yN()
    error_experiment_y0()