"""
MTMW12 Assignment 3
Laura Risley
18th October 2021
Python code to numerically differentiate the pressure in order to calculate
the geostrophic wind relation using 2-point differences and compare with the 
analytic solution and plot
"""

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *

def geostrophicWind():
    '''
    Calculate the geostrophic wind analytically, numerically and plot
    '''
    
    N = 10  #the number of intervals to divide the space into
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin)/N    #the length of the spacing
    
    #The spatial dimension, y:
        
    y = np.linspace(ymin,ymax, N+1)
    
    #The pressure at the y points and the exact geostrophic wind
    
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)
                       
    #The pressure gradient and wind using using two point differences
    
    dpdy = gradient_2point(p, dy)
    u_2point = geoWind(dpdy, physProps)
    
    #Graph to compare the numerical and analytic solutions
    
    #plot using large fonts
    font = {'size' : 14}
    plt.rc('font',**font)
    
    #Plot the approximate and exact wind at y-points
    plt.plot(y/1000, uExact, 'k-', label = 'Exact')
    plt.plot(y/1000, u_2point, '*g--', label='Two-point differences', \
             ms=12, markeredgewidth = 1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y (km)') 
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.title('Numerical and Analytical solutions of the wind')
    plt.savefig('geoWindCent.pdf')
    plt.show()
    
    #Plot the errors
    plt.plot(y/1000, u_2point - uExact, 'ok-', label = 'Two-point differences',
             ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.xlabel('y (km)') 
    plt.ylabel('u, error (m/s)')
    plt.tight_layout()
    plt.title('Errors of the numerical solution')
    plt.savefig('geoWindErrorsCent.pdf')
    plt.show()


if __name__ == "__main__":
    geostrophicWind()
